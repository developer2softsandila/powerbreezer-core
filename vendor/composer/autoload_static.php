<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb7c56cc817619b67cc802c494b5b3de4
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PowerBreezerCore\\Inc\\' => 21,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PowerBreezerCore\\Inc\\' => 
        array (
            0 => __DIR__ . '/../..' . '/inc',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb7c56cc817619b67cc802c494b5b3de4::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb7c56cc817619b67cc802c494b5b3de4::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
