jQuery(document).ready(function($)
{
    $(".read_more").click(function(e){
        e.preventDefault();
        var id = $(this).attr("data-id");
        console.log( id );
        $("img#loader_"+id).show();
        $("img#loader_"+id).css("display", "inherit");
        console.log( "img#loader_"+id);
        $.ajax ({
            url: "/entrapaulus/wp-admin/admin-ajax.php",
            type: "POST",
            dataType: "JSON",
            data: {
                // the value of data.action is the part AFTER 'wp_ajax_' in
                // the add_action ('wp_ajax_xxx', 'yyy') in the PHP above
                action: "read_more",
                // ANY other properties of data are passed to your_function()
                // in the PHP global $_REQUEST (or $_POST in this case)
                id : id
            },

            success: function (resp) {
                if (resp) {
                    // if you wanted to use the return value you set
                    // in your_function(), you would do so via
                    // resp.data, but in this case I guess you don't
                    // need it
                    console.log(resp);
                    // var html = '';
                    // $.each(resp.data, function( index, value ) {
                    //     html+=value;
                    // });
                    $("img#loader_"+id).hide();
                    $("#read-ajax-model h4").html(resp.post_title);
                    $("#read-ajax-model p").html(resp.post_content);
                    jQuery("body").addClass("noscoll");
                    jQuery(".overlay").show();
                    jQuery("#read-ajax-model").show();

                    // $('#product-' + productID + ' .item-product-footer-vote-container').html ('Thanks for your vote!') ;
                }
                else {
                    // this "error" case means the ajax call, itself, succeeded, but the function
                    // called returned an error condition
                    alert ("Error: " + resp.data) ;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // this error case means that the ajax call, itself, failed, e.g., a syntax error
                // in your_function()
                alert ("Request failed: " + thrownError.message) ;
            },
        }) ;
    });



});
