<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Shortcodes;

use PowerBreezerCore\Inc\Base\BaseController;

class Testimonials extends BaseController
{
    public function register()
    {
        add_shortcode ( 'testimonial' , array( $this , 'testimonial_shortcode_handler') );
    }

    public function testimonial_shortcode_handler( $atts ){
        $order = isset ( $atts['order'] )  ? $atts['order'] : 'DESC';
        ob_start();
        $args = array(
            'post_type' => 'testimonials',
            'order' => $order,
        );

        $testimonials_posts = get_posts( $args );

        if ( $testimonials_posts ) :
            ?>
            <div class="testimonils-slider">
                <?php
                foreach ( $testimonials_posts as $post ) :
                    setup_postdata( $post ); ?>
                    <div class="testimonils-slide">

                        <div class="row">
                            <div class="col-md-3">
                                <?php $url = get_the_post_thumbnail_url( $post->ID, 'full' ); ?>
                                <img src="<?php echo $url;?>" />
                            </div>
                            <div class="col-md-9">
                                <?php the_content();?>
                                <div class="matainfo">
                                    <strong>-SGT Harold Presley,</strong>
                                    <?php echo get_post_meta ( $post->ID , 'designation' , true ) ;?>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php
                endforeach;
                wp_reset_postdata();
                ?>

            </div>

        <?php
        endif;
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }

}
