<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Shortcodes;

use PowerBreezerCore\Inc\Base\BaseController;

class Contact_Form extends BaseController
{
    function register()
    {
        add_shortcode( 'breeze_form' , array( $this , 'wp_breeze_form_shortcode' ) );
    }

    function wp_breeze_form_shortcode(){
        ?>
        <div role="form" class="wpcf7" id="wpcf7-f231-p230-o1" lang="en-US" dir="ltr">
            <div class="screen-reader-response" role="alert" aria-live="polite"></div>
            <form action="/entrapaulus/breez-form/#wpcf7-f231-p230-o1" method="post" class="wpcf7-form init" novalidate="novalidate">
                <div style="display: none;">
                    <input type="hidden" name="_wpcf7" value="231">
                    <input type="hidden" name="_wpcf7_version" value="5.2">
                    <input type="hidden" name="_wpcf7_locale" value="en_US">
                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f231-p230-o1">
                    <input type="hidden" name="_wpcf7_container_post" value="230">
                    <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="input-type-text">
                            <label>Enter Site Employees*</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap site-employees"><input type="text" name="site-employees" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>How many works in Non AC Environments *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap no-ac-environments"><input type="text" name="no-ac-environments" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-4">
                        <div class="input-type-text full-width-input">
                            <label>Number of Shifts *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap no-shifts"><input type="text" name="no-shifts" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-4">
                        <div class="input-type-text full-width-input">
                            <label>Choose your Heat Zone *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap choose-your-heat-zone"><input type="text" name="choose-your-heat-zone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-4">
                        <div class="input-type-text full-width-input">
                            <label>Works days per week *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap work-days-per-week"><input type="text" name="work-days-per-week" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>Estimated Average Hourly Cost *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap estimated-average-hourly-cost"><input type="text" name="estimated-average-hourly-cost" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>What areas are hot at your site ? *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap what-ares-hot-at-your-site"><input type="text" name="what-ares-hot-at-your-site" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>Uncooled Square Feet of Facility *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap uncooled-square-feet"><input type="text" name="uncooled-square-feet" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>Estimate Ceiling Height (30 , 36 , 40 ) *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap estimated-ceiling-height"><input type="text" name="estimated-ceiling-height" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>Average Summer Temp in Facility *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap average-summer-temp"><input type="text" name="average-summer-temp" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-md-6"></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>Full Name*</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap full-name"><input type="text" name="full-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>Company *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap company"><input type="text" name="company" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>City *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap city"><input type="text" name="city" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>State *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap city"><input type="text" name="city" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>Business Email *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap business-email"><input type="email" name="business-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-6">
                        <div class="input-type-text full-width-input">
                            <label>Phone ( Optional ) *</label><p></p>
                            <div class="input-text">
                                <span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
                            </div>
                            <p></p></div>
                        <p></p></div>
                    <div class="col-md-12">
                        <input type="submit" value="Schedule Heat Mitigation" class="wpcf7-form-control wpcf7-submit"></span>
                    </div>
                </div>
                <div class="wpcf7-response-output" role="alert" aria-hidden="true"></div></form></div>
        <?php
    }
}
