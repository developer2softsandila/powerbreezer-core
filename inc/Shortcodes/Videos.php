<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Shortcodes;

use PowerBreezerCore\Inc\Base\BaseController;

class Videos extends BaseController
{
    public function register()
    {
        add_shortcode( 'videos', array( $this , 'videos_shortcode_handler' ) );
    }

    public function videos_shortcode_handler($atts)
    {
        $order = isset ($atts['order']) ? $atts['order'] : 'DESC';
        ob_start();
        $args = array(
            'post_type' => 'videos',
            'order' => $order,
        );

        $videos_posts = get_posts($args);
        $i = 1;
        $k = 1;
        $j = 1;
        if ($videos_posts) :
            foreach ($videos_posts as $post)  :
                if ($i > 1) {
                    if ($k == 2):
                        ?>
                        <div class="col-sm-3">
                        <ul class="video-box-nav">
                    <?php endif ?>
                    <li class="active" id="video1">
                    <!--                            <img src="--><?php //echo get_the_post_thumbnail_url($post->ID, 'full'); ?><!--"></li>-->
                    <?php echo get_post_meta($post->ID, 'iframe', TRUE); ?>

                    <?php
                }
                if ($i == 1) {
                    if ( $j==1 ) :
                        ?>
                        <div class="col-sm-9">
                        <div class="video-box">
                    <?php
                    endif;
                    ?>
                    <div id="video1" class="video-box-div active">
                        <!--                                <img src="--><?php //echo get_the_post_thumbnail_url($post->ID, 'full'); ?><!--">-->
                        <?php echo get_post_meta($post->ID, 'iframe', TRUE); ?>
                    </div>
                    <!--                    <div  id="video2" class="video-box-div">-->
                    <!--                        <img src="http://demo.nayyerraza.com/entrapaulus/wp-content/themes/powerbreeza/assets/images/video.jpg">-->
                    <!--                    </div>-->
                    <!--                    <div  id="video3" class="video-box-div">-->
                    <!--                        <img src="http://demo.nayyerraza.com/entrapaulus/wp-content/themes/powerbreeza/assets/images/video.jpg">-->
                    <!--                    </div>-->
                    <!--                    <div  id="video4" class="video-box-div">-->
                    <!--                        <img src="http://demo.nayyerraza.com/entrapaulus/wp-content/themes/powerbreeza/assets/images/video.jpg">-->
                    <!--                    </div>-->
                    <!--                    <div id="video5" class="video-box-div">-->
                    <!--                        <img src="http://demo.nayyerraza.com/entrapaulus/wp-content/themes/powerbreeza/assets/images/video.jpg">-->
                    <!--                    </div>-->
                    <!--                    <div id="video6" class="video-box-div">-->
                    <!--                        <img src="http://demo.nayyerraza.com/entrapaulus/wp-content/themes/powerbreeza/assets/images/video.jpg">-->
                    <!--                    </div>-->
                    <?php if ( $j == 1 ) : ?>
                        </div>
                        </div>
                    <?php
                    endif;
                }

                $i++;
                $k++;
                $j++;

            endforeach;
            wp_reset_postdata();
            if ($k == 2):
                ?>
                </ul>
                </div>
            <?php
            endif;
        endif;
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }

}
