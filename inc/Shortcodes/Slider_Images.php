<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Shortcodes;

use PowerBreezerCore\Inc\Base\BaseController;

class Slider_Images extends BaseController
{
    public function register()
    {
        add_shortcode ( 'slider_image' , array ( $this , 'slider_image_handler') );
    }

    public function slider_image_handler( $atts ) {
        $order = isset ( $atts['order'] )  ? $atts['order'] : 'DESC';
        ob_start();
        $args = array(
            'post_type' => 'sliderimage',
            'order' => $order,
        );

        $sliderimage_posts = get_posts( $args );

        if ( $sliderimage_posts ) :
            ?>
            <ul class="images-grid-slider">
                <?php
                foreach ( $sliderimage_posts as $post ) :
                    setup_postdata( $post ); ?>
                    <?php $url = get_the_post_thumbnail_url( $post->ID, 'full' );
                    if ( $url ) :
                        ?>
                        <li><img src="<?php echo $url;?>"></li>
                    <?php
                    endif;
                endforeach;
                wp_reset_postdata();
                ?>
            </ul>
        <?php
        endif;
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }

}
