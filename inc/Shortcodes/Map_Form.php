<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Shortcodes;

use PowerBreezerCore\Inc\Base\BaseController;

class Map_Form extends BaseController
{
    function register()
    {
        add_shortcode( 'map_form' , array( $this , 'wp_map_form_shortcode' ) );
    }

    function wp_map_form_shortcode(){
        ?>
        <div class="section timeline two-col-section contact-section no-mar-bottom" id="section5">
            <h2 class="text-center pb-4">Find Out What Heat Is Costing You</h2>
            <div class="section-inner">
                <div class="row">
                    <div class="col-sm-6 ">
                        <div class="timeline">

                            <p>Your custom cost of heat analysis</p>
                            <img src="http://demo.nayyerraza.com/entrapaulus/wp-content/themes/powerbreeza/assets/images/bg-map.png">

                        </div>
                    </div>
                    <div class="col-sm-6 full-column form-cont">
                        <div class="contact-form">



                            <h3>Facility</h3>
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-type-text">
                                        <label>Address *</label>
                                        <div class="input-text"><input type="text" placeholder="Site size in sqf"></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-type-text full-width-input">
                                        <label>Site size in sqf *</label>
                                        <div class="input-text"><input type="text" placeholder="Site size in sqf"></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-type-text full-width-input">
                                        <label>Site size in sqf *</label>
                                        <div class="input-text"><input type="text" placeholder="Site size in sqf"></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-type-text full-width-input">
                                        <label>Site size in sqf *</label>
                                        <div class="input-text"><select class="custom-select custom-select-lg">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-type-text full-width-input">
                                        <label>Site size in sqf *</label>
                                        <div class="input-text"><input type="text" placeholder="Site size in sqf"></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-type-text full-width-input">
                                        <label>Site size in sqf *</label>
                                        <div class="input-text"><input type="text" placeholder="Site size in sqf"></div>
                                    </div>
                                </div>


                            </div>



                            <div class="row">
                                <div class="col-6">
                                    <div class="input-type-text full-width-input">
                                        <label>Name *</label>
                                        <div class="input-text"><select class="custom-select custom-select-lg">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select></div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-type-text full-width-input">
                                        <label>Email *</label>
                                        <div class="input-text"><select class="custom-select custom-select-lg">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select></div>
                                    </div>
                                </div>


                            </div>





                            <button>Schedule Heat Mitigation</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php
    }
}
