<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Shortcodes;

use PowerBreezerCore\Inc\Base\BaseController;

class Tabs extends BaseController
{
    function register()
    {
        add_shortcode ( 'tabs' ,  array ( $this , 'wp_tabs_shortcode') );
    }

    public function wp_tabs_shortcode(){
        ob_start();
        ?>
        <div class='section timeline three-col-section no-mar-bottom paddding-right' id="section7">
            <div class='section-inner'>
                <h2 class='text-center'>Cooling Where No One Else Can</h2></br>
                <div class='row slide-wrapper d-none'>
                    <div class='col-sm-4'>
                        <img src='<?php echo get_template_directory_uri(); ?>/assets/images/basketball-match.jpg' />
                        <h6 class='small-heading'> All 5 Branches of Military </h6>
                    </div>
                    <div class='col-sm-4'>
                        <img src='<?php echo get_template_directory_uri(); ?>/assets/images/basketball-match.jpg' />
                        <h6 class='small-heading'>NFL, MLB, NCAA teams  </h6>
                    </div>
                    <div class='col-sm-4'>
                        <img src='<?php echo get_template_directory_uri(); ?>/assets/images/basketball-match.jpg' />
                        <h6 class='small-heading'>Fortune 500 companies  </h6>
                    </div>
                </div>


                <div class="tabs extra-section">

                    <div class="tab-head-main">
                        <ul>
                            <li id="tab-main-1" class="active" data-id="tab1"><h3><?php echo get_option('tab1');?></h3></li>
                            <li id="tab-main-2" data-id="tab2"><h3><?php echo  get_option('tab2');?></h3></li>
                            <li id="tab-main-3" data-id="tab3"><h3><?php echo  get_option('tab3');?></h3></li>
                            <li id="tab-main-4" data-id="tab4"><h3><?php echo  get_option('tab4');?></h3></li>
                        </ul>
                    </div>

                    <div class="tabs-container">
                        <div id="tab-main-1" class="tab-content-main active">


                            <ul class="tabs-head">
                                <li id="tab1" class="active"><h3>What they are saying</h3></li>
                                <li id="tab2" ><h3>Cool views</h3></li>
                                <li id="tab3" ><h3>Breezers in action</h3></li>
                            </ul>


                            <div id="tab1" class="tab-content active">

                                <div class="testimonils-slider">

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>




                                </div>


                            </div>


                            <div id="tab2" class="tab-content">
                                <ul class="images-grid-slider">
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-1.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-2.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-3.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-1.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-2.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-3.png"></li>
                                </ul>
                            </div>
                            <div id="tab3" class="tab-content">
                                <div class="row row-reverse">
                                    <div class="col-sm-3">
                                        <ul class="video-box-nav">
                                            <li class="active" id="video1"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video2"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video4"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video5"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video6"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                        </ul>

                                    </div>
                                    <div class="col-sm-9">
                                        <div class="video-box">
                                            <div id="video1" class="video-box-div active">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video2" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video3" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video4" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div id="video5" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div id="video6" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div id="tab-main-2" class="tab-content-main">



                            <ul class="tabs-head">
                                <li id="tab4" class="active"><h3>What they are saying</h3></li>
                                <li id="tab5" ><h3>Cool views</h3></li>
                                <li id="tab6" ><h3>Breezers in action</h3></li>
                            </ul>


                            <div id="tab4" class="tab-content active">

                                <div class="testimonils-slider">

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>




                                </div>


                            </div>


                            <div id="tab5" class="tab-content">
                                <ul class="images-grid-slider">
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-1.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-2.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-3.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-1.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-2.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-3.png"></li>
                                </ul>
                            </div>
                            <div id="tab6" class="tab-content">
                                <div class="row row-reverse">
                                    <div class="col-sm-3">
                                        <ul class="video-box-nav">
                                            <li class="active" id="video1"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video2"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video4"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video5"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video6"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                        </ul>

                                    </div>
                                    <div class="col-sm-9">
                                        <div class="video-box">
                                            <div id="video1" class="video-box-div active">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video2" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video3" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video4" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div id="video5" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div id="video6" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div id="tab-main-3" class="tab-content-main">



                            <ul class="tabs-head">
                                <li id="tab7" class="active"><h3>What they are saying</h3></li>
                                <li id="tab8" ><h3>Cool views</h3></li>
                                <li id="tab9" ><h3>Breezers in action</h3></li>
                            </ul>


                            <div id="tab7" class="tab-content active">

                                <div class="testimonils-slider">

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>




                                </div>


                            </div>


                            <div id="tab8" class="tab-content">
                                <ul class="images-grid-slider">
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-1.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-2.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-3.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-1.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-2.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-3.png"></li>
                                </ul>
                            </div>
                            <div id="tab9" class="tab-content">
                                <div class="row row-reverse">
                                    <div class="col-sm-3">
                                        <ul class="video-box-nav">
                                            <li class="active" id="video1"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video2"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video4"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video5"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video6"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                        </ul>

                                    </div>
                                    <div class="col-sm-9">
                                        <div class="video-box">
                                            <div id="video1" class="video-box-div active">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video2" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video3" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video4" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div id="video5" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div id="video6" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div id="tab-main-4" class="tab-content-main">



                            <ul class="tabs-head">
                                <li id="tab10" class="active"><h3>What they are saying</h3></li>
                                <li id="tab11" ><h3>Cool views</h3></li>
                                <li id="tab12" ><h3>Breezers in action</h3></li>
                            </ul>


                            <div id="tab10" class="tab-content active">

                                <div class="testimonils-slider">

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="testimonils-slide">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonils.png">

                                            </div>
                                            <div class="col-md-9">
                                                <p>"These Breezers are working great. There is actually less film after disinfecting the housing areas then we expected.
                                                    Also their portability is an asset for storage and movement. We will continue to use the Breezer even after the
                                                    COVID-19 epidemic to provide cooling to other areas as needed."</p>
                                                <div class="matainfo">
                                                    <strong>-SGT Harold Presley,</strong>
                                                    Sanford Correctional Center
                                                </div>
                                            </div>
                                        </div>

                                    </div>




                                </div>


                            </div>


                            <div id="tab11" class="tab-content">
                                <ul class="images-grid-slider">
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-1.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-2.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-3.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-1.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-2.png"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-3.png"></li>
                                </ul>
                            </div>
                            <div id="tab12" class="tab-content">
                                <div class="row row-reverse">
                                    <div class="col-sm-3">
                                        <ul class="video-box-nav">
                                            <li class="active" id="video1"><img src="assets/images/video.jpg"></li>
                                            <li id="video2"><img src="<?php echo get_template_directory_uri(); ?>/<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video4"><img src="<?php echo get_template_directory_uri(); ?>/<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video5"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                            <li id="video6"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg"></li>
                                        </ul>

                                    </div>
                                    <div class="col-sm-9">
                                        <div class="video-box">
                                            <div id="video1" class="video-box-div active">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video2" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video3" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div  id="video4" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div id="video5" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                            <div id="video6" class="video-box-div">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/video.jpg">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>

                    </div>


                </div>



            </div>
        </div>
        <?php
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }
}
