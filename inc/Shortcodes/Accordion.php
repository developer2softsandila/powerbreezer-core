<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Shortcodes;

use PowerBreezerCore\Inc\Base\BaseController;

class Accordion extends BaseController
{
    function register()
    {
        add_shortcode ( 'accordion' ,  array ( $this , 'wp_accordion_shortcode') );
    }

    public function wp_accordion_shortcode( $atts ){
        ob_start();
        $order = isset ( $atts['order'] )  ? $atts['order'] : 'ASC';
        $slug = isset( $atts['category']) ? $atts['category'] : 'section-two';
        ob_start();
        $args = array(
            'post_type' => 'accordions',
            'posts_per_page' => -1,
            'order' => $order,
            'tax_query' => array (
                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => $slug,
                )
            )
        );

        $accordions_posts = get_posts( $args );
//        echo '<pre>';
//        print_r ( $accordions_posts );
//        die;
        $count= 1;
        ?>
        <?php foreach ( $accordions_posts as $accordions_post ) :
            if ( $count == 1 ) {
                $class = 'class="active"';
                $style = "";

            }
            else {
                $class = "";
                $style = 'style="display:none"';

            }
            ?>
                <li <?php echo $class;?>>
                    <h3><?php echo $accordions_post->post_title; ?></h3>
                    <h4><?php echo get_post_meta($accordions_post->ID , 'sub_title' , true );?></h4>
                    <div class='timline-accordion-content' <?php echo $style; ?>>
                        <div class="timline-wrapper">

                            <div class="timline-text">

                                <a class="btn btn-outline read_more" href="javascript:void(0)" data-id="<?php echo $accordions_post->ID;?>">Read more</a><img id="loader_<?php echo $accordions_post->ID;?>" src="<?php echo $this->plugin_url . 'assets/img/Pulse.gif'?>" width="50" style="margin-left: 10px;height: 40px;display: none;" />

                            </div>
                            <div class="timline-embed">
                                <?php echo get_post_meta( $accordions_post->ID , 'video_embed_url' , true );?>
                            </div>
                        </div>
                    </div>
                </li>
        <?php
        $count++;
        endforeach;?>
        <?php
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }

}
