<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Pages;

use PowerBreezerCore\Inc\Base\BaseController;
use PowerBreezerCore\Inc\Api\SettingsApi;

class Admin extends BaseController {

	public function register() {
        add_action("admin_menu", array ( $this , "add_theme_menu_item" ) );
        add_action("admin_init", array  ( $this , "shortcode_settings_fields" ) );
        add_action("admin_init", array  ( $this , "tabs_settings_fields" ) );
        add_action("init" , array ( $this , 'widgets_loaded' ) );

    }

    /***
     *
     * Social Functions
     *
     */

    function display_twitter_element()
    {
        ?>
        <input type="text" name="twitter_url" style="width: 40%;" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>" />
        <?php
    }

    function display_facebook_element()
    {
        ?>
        <input type="text" name="facebook_url" style="width: 40%;" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
        <?php
    }

    function display_linkedin_element()
    {
        ?>
        <input type="text" name="linkedin_url" style="width: 40%;" id="linkedin_url" value="<?php echo get_option('linkedin_url'); ?>" />
        <?php
    }


    /***
     *
     * Banner Functions
     *
     */


    function display_banner_link()
    {
        ?>
        <input type="text" name="banner_link" style="width: 40%;" id="banner_link" value="<?php echo get_option('banner_link'); ?>" />
        <?php
    }

    function display_logo_image()
    {
        ?>
        <input id="logo_image" type="text" size="36" style="width: 40%;"  name="logo" value="<?php echo get_option('logo'); ?>" />
        <input id="logo_image_button" class="button" type="button" value="Upload Logo" />
        <?php
    }

    function display_banner_image()
    {
        ?>
        <input id="upload_image" type="text" size="36" style="width: 40%;"  name="my_image" value="<?php echo get_option('my_image'); ?>" />
        <input id="upload_image_button" class="button" type="button" value="Upload Image" />
        <?php
    }


    function  tab_1_title_element(){
        ?>
        <input type="text" name="tab1" style="width: 40%;" id="tab1" value="<?php echo get_option('tab1'); ?>" />
        <?php
    }

    function  tab_2_title_element(){
        ?>
        <input type="text" name="tab2" style="width: 40%;" id="tab2" value="<?php echo get_option('tab2'); ?>" />
        <?php
    }

    function  tab_3_title_element(){
        ?>
        <input type="text" name="tab3" style="width: 40%;" id="tab3" value="<?php echo get_option('tab3'); ?>" />
        <?php
    }

    function  tab_4_title_element(){
        ?>
        <input type="text" name="tab4" style="width: 40%;" id="tab4" value="<?php echo get_option('tab4'); ?>" />
        <?php
    }

    /**
     *
     *  ShortCode Settings Fields
     *
     */

    function shortcode_settings_fields()
    {
        /****
         *  Social Settings
         */


        add_settings_section("section", "Social Settings", null , "theme-options");

        add_settings_field("twitter_url", "Twitter Url", array ( $this, "display_twitter_element" ) , "theme-options", "section");
        add_settings_field("facebook_url", "Facebook Url", array ( $this , "display_facebook_element" ), "theme-options", "section");
        add_settings_field("linkedin_url", "Linkedin Url", array ( $this , "display_linkedin_element" ), "theme-options", "section");

        register_setting("section", "twitter_url");
        register_setting("section", "facebook_url");
        register_setting("section", "linkedin_url");

        /****
         *  Banner Settings
         */

        add_settings_field("banner_link", "Banner link", array ( $this , "display_banner_link" ), "theme-options", "section");

        register_setting("section", "banner_link");



        add_settings_field("logo", "Logo", array ( $this , "display_logo_image" ) , "theme-options", "section");

        register_setting("section", "logo");


        add_settings_field("my_image", "Banner Image", array ( $this , "display_banner_image" ) , "theme-options", "section");

        register_setting("section", "my_image");


    }


    /***
     *
     *  Tabs Settings Fields
     *
     */

    function tabs_settings_fields(){

        /***
         *
         *  Tab 1 Title
         *
         */

        add_settings_section("tabs-section", "Tabs Section", null , "tabs-theme-options");

        add_settings_field("tab1", "Tab 1 Title", array ( $this, "tab_1_title_element" ) , "tabs-theme-options", "tabs-section");

        register_setting("tabs-section", "tab1");


        /***
         *
         *  Tab 2 Title
         *
         */

        add_settings_section("tabs-section", "Tabs Section", null , "tabs-theme-options");

        add_settings_field("tab2", "Tab 2 Title", array ( $this, "tab_2_title_element" ) , "tabs-theme-options", "tabs-section");

        register_setting("tabs-section", "tab2");


        /***
         *
         *  Tab 3 Title
         *
         */


        add_settings_section("tabs-section", "Tabs Section", null , "tabs-theme-options");

        add_settings_field("tab3", "Tab 3 Title", array ( $this, "tab_3_title_element" ) , "tabs-theme-options", "tabs-section");

        register_setting("tabs-section", "tab3");

        /***
         *
         *  Tab 4 Title
         *
         */

        add_settings_section("tabs-section", "Tabs Section", null , "tabs-theme-options");

        add_settings_field("tab4", "Tab 4 Title", array ( $this, "tab_4_title_element" ) , "tabs-theme-options", "tabs-section");

        register_setting("tabs-section", "tab4");


    }




    function add_theme_menu_item()
    {
        add_menu_page("Shortcode Plugin", "Shortcode Plugin", "manage_options", "shortcode-core-plugin", array ( $this , "shortcode_core_settings_page" ), null, 99);
        add_submenu_page("shortcode-core-plugin", "Shortcode Plugin", "Shortcode Plugin" , "manage_options", "shortcode-core-plugin" );
        add_submenu_page("shortcode-core-plugin", "Tabs Settings", "Tabs Settings" , "manage_options", "tabs-settings-page", array ( $this , "tabs_settings_page" ), null );

    }
    /**
     * main settings page
    **/

    function shortcode_core_settings_page(){
        ?>
        <div class="wrap">
            <h1>Shortcode Core Settings Page</h1>
            <form method="post" action="options.php">
                <?php
                settings_fields("section");
                do_settings_sections("theme-options");
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }


    /**
     * tabs settings page
     **/

    function tabs_settings_page(){
        ?>
        <div class="wrap">
            <h1>Tabs Settings Page</h1>
            <form method="post" action="options.php">
                <?php
                settings_fields("tabs-section");
                do_settings_sections("tabs-theme-options");
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    function widgets_loaded(){
        register_sidebar( array(
            'name' => 'Footer',
            'id' => 'footer',
            'description' => 'Appears in the footer area',
            'before_widget' => '<div id="%1$s section9" class="widget %2$s section timeline three-col-section no-mar-bottom paddding-right prodcut-section">',
            'after_widget' => '</div>',
            'before_title' => '',
            'after_title' => '',
        ) );
    }






}
