<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Pages;

use PowerBreezerCore\Inc\Base\BaseController;

class User extends BaseController {

    function register() {
        add_action('wp_ajax_read_more', array ( $this , 'read_more_popup'  ) );
        add_action('wp_ajax_nopriv_read_more', array ( $this , 'read_more_popup' ) );

        add_action('wp_ajax_tabs_ajax_load', array ( $this , 'tabs_ajax_load_script'  ) );
        add_action('wp_ajax_nopriv_tabs_ajax_load', array ( $this , 'tabs_ajax_load_script') );

    }


    function read_more_popup()
    {
        if ( isset ( $_REQUEST['id'] ) && !empty( $_REQUEST ) ) {
            $post = get_post( $_REQUEST['id'] );
            header('Content-Type: application/json');
            echo json_encode($post);
        }
        die();
    }

    function tabs_ajax_load_script()
        {
            if ( isset ( $_REQUEST['id'] ) && !empty( $_REQUEST ) ) {
                $post = get_post( $_REQUEST['id'] );
                header('Content-Type: application/json');
                echo json_encode($post);
            }
            die();
        }


}
