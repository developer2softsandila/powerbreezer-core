<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Base;

class Deactivate {

	public static function deactivate(){
		flush_rewrite_rules();
	}
}

