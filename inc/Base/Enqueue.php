<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\Base;

use \PowerBreezerCore\Inc\Base\BaseController;

class Enqueue extends BaseController{

    function register() {
        add_action('wp_enqueue_scripts', array  ( $this , 'custom_enqueue' ) );
        add_action ( 'admin_enqueue_scripts'  , array ( $this , 'admin_enqueue' ) );


    }

    function admin_enqueue(){
        wp_enqueue_style( 'popup-style' , $this->plugin_url . 'assets/css/popup-style.css' , '' ,'1.0' ,true );
    }

    function custom_enqueue(){
        wp_enqueue_script('readmore-popup-ajax-script', $this->plugin_url . 'assets/js/read-more-popup.js', array('jquery'), '1.0.0', 'true' );
        wp_enqueue_script('tabs-ajax-script', $this->plugin_url . 'assets/js/tabs-script.js', array('jquery'), '1.0.0', 'true' );
    }
}
