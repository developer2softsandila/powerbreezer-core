<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\CPT;

use PowerBreezerCore\Inc\Base\BaseController;

class Slider_Image extends BaseController
{
    function register()
    {
        add_action( 'init',  array ( $this , 'wp_slider_image' ) );
    }

    /**********
     *
     *      Slider image custom post
     *
     ***********/


    function wp_slider_image() {
        $labels = array(
            'name'                  => _x( 'Slider Image', 'Post type general name', 'recipe' ),
            'singular_name'         => _x( 'Slider Image', 'Post type singular name', 'recipe' ),
            'menu_name'             => _x( 'Slider Image', 'Admin Menu text', 'recipe' ),
            'name_admin_bar'        => _x( 'Slider Image', 'Add New on Toolbar', 'recipe' ),
            'add_new'               => __( 'Add New', 'recipe' ),
            'add_new_item'          => __( 'Add New Slider Image', 'recipe' ),
            'new_item'              => __( 'New Slider Image', 'recipe' ),
            'edit_item'             => __( 'Edit Slider Image', 'recipe' ),
            'view_item'             => __( 'View Slider Image', 'recipe' ),
            'all_items'             => __( 'All Slider Image', 'recipe' ),
            'search_items'          => __( 'Search Slider Image', 'recipe' ),
            'parent_item_colon'     => __( 'Parent Slider Image:', 'recipe' ),
            'not_found'             => __( 'No tabs found.', 'recipe' ),
            'not_found_in_trash'    => __( 'No tabs found in Trash.', 'recipe' ),
            'featured_image'        => _x( 'Slider Image Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'recipe' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'recipe' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'recipe' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'recipe' ),
            'archives'              => _x( 'Slider Image archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'recipe' ),
            'insert_into_item'      => _x( 'Insert into tab', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'recipe' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this Slider Image', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'recipe' ),
            'filter_items_list'     => _x( 'Filter Slider Image list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'recipe' ),
            'items_list_navigation' => _x( 'Slider Image list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'recipe' ),
            'items_list'            => _x( 'Slider Image list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'recipe' ),
        );
        $args = array(
            'labels'             => $labels,
            'description'        => 'Videos.',
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'wp_slider_image' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => 20,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail' ),
            'show_in_rest'       => true
        );

        register_post_type( 'Slider Image', $args );
    }


}




