<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\CPT;

use PowerBreezerCore\Inc\Base\BaseController;

class Metabox extends BaseController
{

    function register()
    {
        add_filter( 'rwmb_meta_boxes', array ( &$this, 'meta_box_tabs_register' ) );

    }


    public function wpse46583_save( $post_id, $post ) {

        // verify this is not an auto save routine.
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
            return;

        // You should check nonces and permissions here
        if ( ! current_user_can( 'edit_page', $post_id ) )
            return;

        if ( $_REQUEST['page_template'] == 'page-contact.php' ) {
            // No page template assigned - do something here.

        }

        return;
    }


    public function meta_box_tabs_register( $meta_boxes )
    {

        global $wpdb;

        $meta_boxes[] = array(
            'title' => 'Detail Info',
            'post_types' => 'accordions',
            'fields' => array(
                array(
                    'name'        => 'Sub Title',
                    'id'          => 'sub_title',
                    'desc'        => 'Please enter Sub Title',
                    'type'        => 'text',
                    // Placeholder
                    'placeholder' => 'Enter Sub Title',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Video Embed URL',
                    'id'          => 'video_embed_url',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Video Embed URL',
                    // Input size
                    'size'        => 30,
                ),
            ),
        );

        $meta_boxes[] = array(
            'title' => 'Detail Info',
            'post_types' => 'testimonials',
            'fields' => array(
                array(
                    'name'        => 'Sub Title',
                    'id'          => 'sub_title',
                    'desc'        => 'Please enter Sub Title',
                    'type'        => 'text',
                    // Placeholder
                    'placeholder' => 'Enter Sub Title',
                    // Input size
                    'size'        => 30,
                ),
            ),
        );


        $meta_boxes[] = array(
            'title' => 'Tabs Options',
            'post_types' => array ( 'testimonials' , 'videos'  , 'sliderimage'  ),
            'fields' => array(
                array(
                    'name'            => 'Select Parent Tab',
                    'id'              => 'select_parent_tab',
                    'type'            => 'select',
                    // Array of 'value' => 'Label' pairs
                    'options'        => array(
                        'tab1'       => 'All 5 branches of military',
                        'tab2'       => 'Your Favorite NFL Teams',
                        'tab3'       => 'Fortune 500 Companies',
                        'tab4'       => 'Corrections Facilities',
                    ),
                    // Allow to select multiple value?
                    'multiple'        => false,
                    // Placeholder text
                    'placeholder'     => 'Select parent tab',
                    // Display "Select All / None" button?
                ),
            ),
        );
        $meta_boxes[] = array(
            'title' => 'Details',
            'post_types' => 'videos',
            'fields' => array(
                array(
                    'name'        => 'Video Embed URL',
                    'id'          => 'video_embed_url',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Video Embed URL',
                    // Input size
                    'size'        => 30,
                ),
            ),
        );

        $meta_boxes[] = array(
            'title' => 'Section 1: Banner',
            'post_types' => 'page',
            'show'   => array(
                // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
                'relation'    => 'OR',

                // List of page templates (used for page only). Array. Optional.
                'template'    => array('template-home.php'),

                // List of post formats. Array. Case insensitive. Optional.
                'post_format' => array(),

                // List of categories IDs or names (NOT slugs). Array. Case sensitive. Optional.
                'category'    => array(),

                // Custom taxonomy. Optional.
                // Format: 'taxonomy' => list of term IDs or names (NOT slugs). Array. Case sensitive. Optional.
//                    'location'    => array( 12, 'USA', 'europe' ),
//                    'os'          => array( 'Windows', 'mac-os' ),

                // Check if page is a child page
//                    'is_child' => true,

                // Check the value of selector. Format: array( selector, value )
                // Added in version 0.2
                'input_value'   => array(
                    '#hide-demo-mb'              => 'yes',
                    '#hide-demo-mb-2'            => 'yes',
                    'input[name=hide-demo-mb-3]' => true, // If it's a checkbox then true == checked
                ),
            ),
            'fields' => array(
                array(
                    'name'        => 'Title',
                    'id'          => 'section_1_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Section 1 Title',
                    // Input size
                    'rows' => 1,
                ),
                array(
                    'name'        => 'Short Detail',
                    'id'          => 'section_1_short_detail',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Section 1 Short Detail',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Video Embed URL',
                    'id'          => 'video_embed_url',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Video Embed URL',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Next Section Title',
                    'id'          => 'next_section_title',
                    'type'        => 'textarea',
                    'width' => '100',
                    // Placeholder
                    'placeholder' => 'Enter Next Section Title',
                    // Input size
                    'rows' => 1,
                ),
            ),
                );

        $meta_boxes[] = array(
            'title' => 'Section 2 : Accordion',
            'post_types' => 'page',
            'show'   => array(
                // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
                'relation'    => 'OR',

                // List of page templates (used for page only). Array. Optional.
                'template'    => array('template-home.php'),

                // List of post formats. Array. Case insensitive. Optional.
                'post_format' => array(),

                // List of categories IDs or names (NOT slugs). Array. Case sensitive. Optional.
                'category'    => array(),

                // Custom taxonomy. Optional.
                // Format: 'taxonomy' => list of term IDs or names (NOT slugs). Array. Case sensitive. Optional.
//                    'location'    => array( 12, 'USA', 'europe' ),
//                    'os'          => array( 'Windows', 'mac-os' ),

                // Check if page is a child page
//                    'is_child' => true,

                // Check the value of selector. Format: array( selector, value )
                // Added in version 0.2
                'input_value'   => array(
                    '#hide-demo-mb'              => 'yes',
                    '#hide-demo-mb-2'            => 'yes',
                    'input[name=hide-demo-mb-3]' => true, // If it's a checkbox then true == checked
                ),
            ),
            'fields' => array(
                array(
                    'name'        => 'Accordion ShortCode',
                    'id'          => 'accordion_shortcode',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Accordion ShortCode',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Next Section Title',
                    'id'          => 'section_2_next_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Next Section Title',
                    // Input size
                    'rows' => 1,
                ),

            ),
        );
        $meta_boxes[] = array(
            'title' => 'Section 3',
            'post_types' => 'page',
            'show'   => array(
                // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
                'relation'    => 'OR',

                // List of page templates (used for page only). Array. Optional.
                'template'    => array('template-home.php'),

                // List of post formats. Array. Case insensitive. Optional.
                'post_format' => array(),

                // List of categories IDs or names (NOT slugs). Array. Case sensitive. Optional.
                'category'    => array(),

                // Custom taxonomy. Optional.
                // Format: 'taxonomy' => list of term IDs or names (NOT slugs). Array. Case sensitive. Optional.
//                    'location'    => array( 12, 'USA', 'europe' ),
//                    'os'          => array( 'Windows', 'mac-os' ),

                // Check if page is a child page
//                    'is_child' => true,

                // Check the value of selector. Format: array( selector, value )
                // Added in version 0.2
                'input_value'   => array(
                    '#hide-demo-mb'              => 'yes',
                    '#hide-demo-mb-2'            => 'yes',
                    'input[name=hide-demo-mb-3]' => true, // If it's a checkbox then true == checked
                ),
            ),
            'fields' => array(
                array(
                    'name'        => 'Title',
                    'id'          => 'section_3_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Title',
                    'rows' => 1,
                ),
                array(
                    'name'        => 'Short Detail',
                    'id'          => 'section_3_short_detail',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Short Detail',
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Video Embed URL',
                    'id'          => 'section_3_video_embed_url',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Video Embed URL',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Next Section Title',
                    'id'          => 'section_3_next_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Next Section Title',
                    // Input size
                    'rows' => 1,
                ),

            ),
        );
        $meta_boxes[] = array(
            'title' => 'Section 4',
            'post_types' => 'page',
            'show'   => array(
                // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
                'relation'    => 'OR',

                // List of page templates (used for page only). Array. Optional.
                'template'    => array('template-home.php'),

                // List of post formats. Array. Case insensitive. Optional.
                'post_format' => array(),

                // List of categories IDs or names (NOT slugs). Array. Case sensitive. Optional.
                'category'    => array(),

                // Custom taxonomy. Optional.
                // Format: 'taxonomy' => list of term IDs or names (NOT slugs). Array. Case sensitive. Optional.
//                    'location'    => array( 12, 'USA', 'europe' ),
//                    'os'          => array( 'Windows', 'mac-os' ),

                // Check if page is a child page
//                    'is_child' => true,

                // Check the value of selector. Format: array( selector, value )
                // Added in version 0.2
                'input_value'   => array(
                    '#hide-demo-mb'              => 'yes',
                    '#hide-demo-mb-2'            => 'yes',
                    'input[name=hide-demo-mb-3]' => true, // If it's a checkbox then true == checked
                ),
            ),
            'fields' => array(
                array(
                    'name'        => 'Title',
                    'id'          => 'section_4_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Title',
                    'rows' => 1,
                ),
                array(
                    'name'        => 'Short Detail',
                    'id'          => 'section_4_short_detail',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Short Detail',
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Video Embed URL',
                    'id'          => 'section_4_video_embed_url',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Video Embed URL',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Next Section Title',
                    'id'          => 'section_4_next_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Next Section Title',
                    // Input size
                    'rows' => 1,
                ),

            ),
        );
        $meta_boxes[] = array(
            'title' => 'Section 5 : Map_Form',
            'post_types' => 'page',
            'show'   => array(
                // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
                'relation'    => 'OR',

                // List of page templates (used for page only). Array. Optional.
                'template'    => array('template-home.php'),

                // List of post formats. Array. Case insensitive. Optional.
                'post_format' => array(),

                // List of categories IDs or names (NOT slugs). Array. Case sensitive. Optional.
                'category'    => array(),

                // Custom taxonomy. Optional.
                // Format: 'taxonomy' => list of term IDs or names (NOT slugs). Array. Case sensitive. Optional.
//                    'location'    => array( 12, 'USA', 'europe' ),
//                    'os'          => array( 'Windows', 'mac-os' ),

                // Check if page is a child page
//                    'is_child' => true,

                // Check the value of selector. Format: array( selector, value )
                // Added in version 0.2
                'input_value'   => array(
                    '#hide-demo-mb'              => 'yes',
                    '#hide-demo-mb-2'            => 'yes',
                    'input[name=hide-demo-mb-3]' => true, // If it's a checkbox then true == checked
                ),
            ),
            'fields' => array(
                array(
                    'name'        => 'Map ShortCode',
                    'id'          => 'map_shortcode',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Map ShortCode',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Contact Form ShortCode',
                    'id'          => 'contact_form_shortcode',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Contact Form ShortCode',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Next Section Title',
                    'id'          => 'section_5_next_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Next Section Title',
                    // Input size
                    'rows' => 1,
                ),

            ),
        );
        $meta_boxes[] = array(
            'title' => 'Section 6 : Accordion',
            'post_types' => 'page',
            'show'   => array(
                // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
                'relation'    => 'OR',

                // List of page templates (used for page only). Array. Optional.
                'template'    => array('template-home.php'),

                // List of post formats. Array. Case insensitive. Optional.
                'post_format' => array(),

                // List of categories IDs or names (NOT slugs). Array. Case sensitive. Optional.
                'category'    => array(),

                // Custom taxonomy. Optional.
                // Format: 'taxonomy' => list of term IDs or names (NOT slugs). Array. Case sensitive. Optional.
//                    'location'    => array( 12, 'USA', 'europe' ),
//                    'os'          => array( 'Windows', 'mac-os' ),

                // Check if page is a child page
//                    'is_child' => true,

                // Check the value of selector. Format: array( selector, value )
                // Added in version 0.2
                'input_value'   => array(
                    '#hide-demo-mb'              => 'yes',
                    '#hide-demo-mb-2'            => 'yes',
                    'input[name=hide-demo-mb-3]' => true, // If it's a checkbox then true == checked
                ),
            ),
            'fields' => array(
                array(
                    'name'        => 'Accordion ShortCode',
                    'id'          => 'section_6_accordion_shortcode',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Accordion ShortCode',
                    // Input size
                    'size'        => 30,
                ),
                array(
                    'name'        => 'Next Section Title',
                    'id'          => 'section_6_next_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Next Section Title',
                    // Input size
                    'rows' => 1,
                ),

            ),
        );

        $meta_boxes[] = array(
            'title' => 'Section 8',
            'post_types' => 'page',
            'show'   => array(
                // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
                'relation'    => 'OR',

                // List of page templates (used for page only). Array. Optional.
                'template'    => array('template-home.php'),

                // List of post formats. Array. Case insensitive. Optional.
                'post_format' => array(),

                // List of categories IDs or names (NOT slugs). Array. Case sensitive. Optional.
                'category'    => array(),

                // Custom taxonomy. Optional.
                // Format: 'taxonomy' => list of term IDs or names (NOT slugs). Array. Case sensitive. Optional.
//                    'location'    => array( 12, 'USA', 'europe' ),
//                    'os'          => array( 'Windows', 'mac-os' ),

                // Check if page is a child page
//                    'is_child' => true,

                // Check the value of selector. Format: array( selector, value )
                // Added in version 0.2
                'input_value'   => array(
                    '#hide-demo-mb'              => 'yes',
                    '#hide-demo-mb-2'            => 'yes',
                    'input[name=hide-demo-mb-3]' => true, // If it's a checkbox then true == checked
                ),
            ),
            'fields' => array(
                array(
                    'name'        => 'Title',
                    'id'          => 'section_8_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Title',
                    // Input size
                    'rows'  => 1
                ),
                array(
                    'name'        => 'Title',
                    'id'          => 'section_8_short_detail',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Short Detail',
                    // Input size
                ),
                array(
                    'name'        => 'Product 1 Title',
                    'id'          => 'section_8_product_1_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Product 1 Title',
                    // Input size
                    'rows'  => 1
                ),

                array(
                    'name'        => 'Product 1 Link',
                    'id'          => 'section_8_product_1_link',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Product 1 Link',
                    // Input size
                    'rows'  => 1
                ),

                array(
                    'id'               => 'product_1_image',
                    'name'             => 'Product 1 Image',
                    'type'             => 'image_advanced',

                    // Delete image from Media Library when remove it from post meta?
                    // Note: it might affect other posts if you use same image for multiple posts
                    'force_delete'     => false,

                    // Maximum image uploads.
                    'max_file_uploads' => 1,

                    // Do not show how many images uploaded/remaining.
                    'max_status'       => 'false',

                    // Image size that displays in the edit page. Possible sizes small,medium,large,original
                    'image_size'       => 'thumbnail',
                ),

                array(
                    'name'        => 'Product 2 Title',
                    'id'          => 'section_8_product_2_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Product 2 Title',
                    // Input size
                    'rows'  => 1
                ),

                array(
                    'name'        => 'Product 2 Link',
                    'id'          => 'section_8_product_2_link',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Product 2 Link',
                    // Input size
                    'rows'  => 1
                ),

                array(
                    'id'               => 'product_2_image',
                    'name'             => 'Product 2 Image',
                    'type'             => 'image_advanced',

                    // Delete image from Media Library when remove it from post meta?
                    // Note: it might affect other posts if you use same image for multiple posts
                    'force_delete'     => false,

                    // Maximum image uploads.
                    'max_file_uploads' => 1,

                    // Do not show how many images uploaded/remaining.
                    'max_status'       => 'false',

                    // Image size that displays in the edit page. Possible sizes small,medium,large,original
                    'image_size'       => 'thumbnail',
                ),

                array(
                    'name'        => 'Product 3 Title',
                    'id'          => 'section_8_product_3_title',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Product 3 Title',
                    // Input size
                    'rows'  => 1
                ),

                array(
                    'name'        => 'Product 3 Link',
                    'id'          => 'section_8_product_3_link',
                    'type'        => 'textarea',
                    // Placeholder
                    'placeholder' => 'Enter Product 3 Link',
                    // Input size
                    'rows'  => 1
                ),

                array(
                    'id'               => 'product_3_image',
                    'name'             => 'Product 3 Image',
                    'type'             => 'image_advanced',

                    // Delete image from Media Library when remove it from post meta?
                    // Note: it might affect other posts if you use same image for multiple posts
                    'force_delete'     => false,

                    // Maximum image uploads.
                    'max_file_uploads' => 1,

                    // Do not show how many images uploaded/remaining.
                    'max_status'       => 'false',

                    // Image size that displays in the edit page. Possible sizes small,medium,large,original
                    'image_size'       => 'thumbnail',
                ),
            ),
        );


        return $meta_boxes;
    }

}




