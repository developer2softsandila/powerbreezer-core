<?php
/**
 * @package PowerBreezerCorePlugin
 */

namespace PowerBreezerCore\Inc\CPT;

use PowerBreezerCore\Inc\Base\BaseController;

class Videos extends BaseController
{
    function register()
    {
        add_action('init', array($this, 'wp_video'));
    }


    /**********
     *
     *      Video custom post
     *
     ***********/


    function wp_video()
    {
        $labels = array(
            'name' => _x('Videos', 'Post type general name', 'recipe'),
            'singular_name' => _x('Video', 'Post type singular name', 'recipe'),
            'menu_name' => _x('Videos', 'Admin Menu text', 'recipe'),
            'name_admin_bar' => _x('Video', 'Add New on Toolbar', 'recipe'),
            'add_new' => __('Add New', 'recipe'),
            'add_new_item' => __('Add New Video', 'recipe'),
            'new_item' => __('New Video', 'recipe'),
            'edit_item' => __('Edit Video', 'recipe'),
            'view_item' => __('View Video', 'recipe'),
            'all_items' => __('All Video', 'recipe'),
            'search_items' => __('Search Videos', 'recipe'),
            'parent_item_colon' => __('Parent Videos:', 'recipe'),
            'not_found' => __('No tabs found.', 'recipe'),
            'not_found_in_trash' => __('No tabs found in Trash.', 'recipe'),
            'featured_image' => _x('Video Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'recipe'),
            'set_featured_image' => _x('Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'recipe'),
            'remove_featured_image' => _x('Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'recipe'),
            'use_featured_image' => _x('Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'recipe'),
            'archives' => _x('Testimonail archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'recipe'),
            'insert_into_item' => _x('Insert into tab', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'recipe'),
            'uploaded_to_this_item' => _x('Uploaded to this Video', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'recipe'),
            'filter_items_list' => _x('Filter Videos list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'recipe'),
            'items_list_navigation' => _x('Videos list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'recipe'),
            'items_list' => _x('Videos list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'recipe'),
        );
        $args = array(
            'labels' => $labels,
            'description' => 'Videos.',
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'wp_video'),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 20,
            'supports' => array('title', 'editor', 'author', 'thumbnail'),
            'show_in_rest' => true
        );

        register_post_type('Videos', $args);
    }


}




